import { MdRssFeed } from "react-icons/md"
import { AiFillTags, AiFillSetting ,AiFillFileAdd} from "react-icons/ai"
import { BsCollection } from "react-icons/bs"


export const Feed = <MdRssFeed className="w-6 h-6 text-gray-800" />
export const Tags = <AiFillTags className="w-6 h-6 text-gray-800" />
export const Collection = <BsCollection className="w-6 h-6 text-gray-800" />
export const NewNote = <AiFillFileAdd className="w-6 h-6 text-gray-800" />
export const Settings = <AiFillSetting className="w-6 h-6 text-gray-800" />
